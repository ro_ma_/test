﻿using Microsoft.AspNetCore.Mvc;
using TestProjectWebAPI.Model;
using TestProjectWebAPI.Repository;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TestProjectWebAPI.Controllers
{
    [Route("api/[controller]")]
    public class ClientController : BaseApiController<Clients>
    { 
       public ClientController(IGenericRepository<Clients> repo) : base(repo) { }
    }
}
