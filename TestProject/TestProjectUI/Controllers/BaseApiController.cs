﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TestProjectWebAPI.Repository;
using System.Net.Http;
using System.Net;
using Newtonsoft.Json;
using System.Text;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TestProjectWebAPI.Controllers
{
    [Route("api/[controller]")]
    public class BaseApiController<EntitySet> : Controller where EntitySet : class, new()
    {
        IGenericRepository<EntitySet> _repo;

        public BaseApiController(IGenericRepository<EntitySet> repo)
        {
            _repo = repo;
        }

         // GET: api/values
        [HttpGet]
        public IEnumerable<EntitySet> Get()
        {
            return _repo.Get().ToList();
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public EntitySet Get(int id)
        {
            return _repo.GetById(id);
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]EntitySet value)
        {
            _repo.Add(value);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]EntitySet value)
        {
            _repo.Update(value);
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
            _repo.Delete(id);
        }
    }

}
