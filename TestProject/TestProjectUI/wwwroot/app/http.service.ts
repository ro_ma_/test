﻿import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Response, Headers} from '@angular/http';
import {Client} from './Client'
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
 
@Injectable()
export class HttpService{
 
    constructor(private http: Http){ }
     
    getClients() : Observable<Client[]>
    {
        return this.http.get('api/Client')
                        .map((resp:Response)=>{
                            let clientList = resp.json();
                            let clients :Client[] = [];
                            for(let index in clientList){
                                let cli = clientList[index];
                                clients.push({id: cli.id, FirstName: cli.firstName, LastName: cli.lastName});
                            }
                            return clients;});
    }

    createClient(obj: Client)
   {
        const body = JSON.stringify(obj);
        let headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });
        return this.http.post('api/Client', body, { headers: headers }); 
   }

     updateClient(id: number, obj: Client) 
    {
        let headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });
        const body = JSON.stringify(obj);
        return this.http.put('api/Client/' + id, body, { headers: headers });
    }

    deleteClient(id: number)
    {
        return this.http.delete('api/Client/' + id);
    }

}