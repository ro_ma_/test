﻿export class Client {
    id: number;
    FirstName: string;
    LastName: string;

    constructor(id: number, FirstName: string, LastName: string) {

        this.id = id;
        this.FirstName = FirstName;
        this.LastName = LastName;
    }
}