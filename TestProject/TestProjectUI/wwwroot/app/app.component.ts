﻿import {TemplateRef, ViewChild} from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { Response } from '@angular/http';
import { HttpService } from './http.service';
import { Client } from './Client';


@Component({
    selector: 'client-app',
    template: `<h1>Список клиентов</h1>
<input type="button" value="Добавить" class="btn btn-default" (click)="addClient()" />
<table class="table table-striped">
    <thead>
        <tr>
          
            <td>Имя</td>
            <td>Фамилия</td>
            <td></td>
            <td></td>
        </tr>
    </thead>
    <tbody>
        <tr *ngFor="let client of items">
            <ng-template [ngTemplateOutlet]="loadTemplate(client)" 
                        [ngOutletContext]="{ $implicit: client}">
            </ng-template>
        </tr>
    </tbody>
</table>
<div>{{statusMessage}}</div>
  
  
<!--шаблон для чтения-->
<ng-template #readOnlyTemplate let-client>
   
    <td>{{client.FirstName}}</td>
    <td>{{client.LastName}}</td>
    <td>
        <input type="button" value="Изменить" class="btn btn-default" (click)="editClient(client)" />
    </td>
    <td>
        <input type="button" value="Удалить" (click)="deleteClient(client)" class="btn btn-danger" />
    </td>
</ng-template>
 
<!--шаблон для редактирования-->
<ng-template #editTemplate>
    
    <td>
        <input type="text" [(ngModel)]="editedClient.FirstName" class="form-control" />
    </td>
    <td>
        <input type="text" [(ngModel)]="editedClient.LastName" class="form-control" />
    </td>
    <td>
        <input type="button" value="Сохранить" (click)="saveClient()" class="btn btn-success" />
    </td>
    <td>
        <input type="button" value="Отмена" (click)="cancel()" class="btn btn-warning" />
    </td>
</ng-template>`,
    providers: [HttpService]
})
export class AppComponent implements OnInit {

    //типы шаблонов
    @ViewChild('readOnlyTemplate') readOnlyTemplate: TemplateRef<any>;
    @ViewChild('editTemplate') editTemplate: TemplateRef<any>;

    editedClient: Client;
    items: Client[] = [];
    isNewRecord: boolean;
    statusMessage: string;

    constructor(private dataService: HttpService) { }

   
   ngOnInit()
{
      this.loadClients();
}

private loadClients()
{
      this.dataService.getClients().subscribe((data)=>this.items=data);
}

 addClient() 
{
        this.editedClient = new Client(0,"","");
        this.items.push(this.editedClient);
        this.isNewRecord = true;
}

editClient(client: Client) 
{
        this.editedClient = new Client(client.id, client.FirstName, client.LastName);
}

// загружаем один из двух шаблонов
loadTemplate(client: Client) 
{
        if (this.editedClient && this.editedClient.id == client.id) 
          {
              return this.editTemplate;
            } 
           else 
            {
              return this.readOnlyTemplate;
            }
}

 saveClient() 
{
        if (this.isNewRecord) 
           {
            // добавляем клиента
            this.dataService.createClient(this.editedClient).subscribe((resp: Response) => {
                this.statusMessage = 'Данные успешно добавлены',
                this.loadClients();
            });
            this.isNewRecord = false;
            this.editedClient = null;
           } 
          else 
           {
            // изменяем клиента
            this.dataService.updateClient(this.editedClient.id, this.editedClient).subscribe((resp: Response) => {
                this.statusMessage = 'Данные успешно обновлены',
                 this.loadClients();
            });
           this.editedClient = null;
        }
  }

 // отмена редактирования
  cancel() 
  {
            if (this.isNewRecord) {
            this.items.pop();
            this.isNewRecord = false;
        }
        this.editedClient = null;
   }

// удаление пользователя
    deleteClient(client: Client) 
  {
        this.dataService.deleteClient(client.id).subscribe((resp: Response) => {
            this.statusMessage = 'Данные успешно удалены',
            this.loadClients();
        });
   }
}