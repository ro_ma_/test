"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var core_2 = require("@angular/core");
var http_service_1 = require("./http.service");
var Client_1 = require("./Client");
var AppComponent = (function () {
    function AppComponent(dataService) {
        this.dataService = dataService;
        this.items = [];
    }
    AppComponent.prototype.ngOnInit = function () {
        this.loadClients();
    };
    AppComponent.prototype.loadClients = function () {
        var _this = this;
        this.dataService.getClients().subscribe(function (data) { return _this.items = data; });
    };
    AppComponent.prototype.addClient = function () {
        this.editedClient = new Client_1.Client(0, "", "");
        this.items.push(this.editedClient);
        this.isNewRecord = true;
    };
    AppComponent.prototype.editClient = function (client) {
        this.editedClient = new Client_1.Client(client.id, client.FirstName, client.LastName);
    };
    // загружаем один из двух шаблонов
    AppComponent.prototype.loadTemplate = function (client) {
        if (this.editedClient && this.editedClient.id == client.id) {
            return this.editTemplate;
        }
        else {
            return this.readOnlyTemplate;
        }
    };
    AppComponent.prototype.saveClient = function () {
        var _this = this;
        if (this.isNewRecord) {
            // добавляем клиента
            this.dataService.createClient(this.editedClient).subscribe(function (resp) {
                _this.statusMessage = 'Данные успешно добавлены',
                    _this.loadClients();
            });
            this.isNewRecord = false;
            this.editedClient = null;
        }
        else {
            // изменяем клиента
            this.dataService.updateClient(this.editedClient.id, this.editedClient).subscribe(function (resp) {
                _this.statusMessage = 'Данные успешно обновлены',
                    _this.loadClients();
            });
            this.editedClient = null;
        }
    };
    // отмена редактирования
    AppComponent.prototype.cancel = function () {
        if (this.isNewRecord) {
            this.items.pop();
            this.isNewRecord = false;
        }
        this.editedClient = null;
    };
    // удаление пользователя
    AppComponent.prototype.deleteClient = function (client) {
        var _this = this;
        this.dataService.deleteClient(client.id).subscribe(function (resp) {
            _this.statusMessage = 'Данные успешно удалены',
                _this.loadClients();
        });
    };
    return AppComponent;
}());
__decorate([
    core_1.ViewChild('readOnlyTemplate'),
    __metadata("design:type", core_1.TemplateRef)
], AppComponent.prototype, "readOnlyTemplate", void 0);
__decorate([
    core_1.ViewChild('editTemplate'),
    __metadata("design:type", core_1.TemplateRef)
], AppComponent.prototype, "editTemplate", void 0);
AppComponent = __decorate([
    core_2.Component({
        selector: 'client-app',
        template: "<h1>\u0421\u043F\u0438\u0441\u043E\u043A \u043A\u043B\u0438\u0435\u043D\u0442\u043E\u0432</h1>\n<input type=\"button\" value=\"\u0414\u043E\u0431\u0430\u0432\u0438\u0442\u044C\" class=\"btn btn-default\" (click)=\"addClient()\" />\n<table class=\"table table-striped\">\n    <thead>\n        <tr>\n          \n            <td>\u0418\u043C\u044F</td>\n            <td>\u0424\u0430\u043C\u0438\u043B\u0438\u044F</td>\n            <td></td>\n            <td></td>\n        </tr>\n    </thead>\n    <tbody>\n        <tr *ngFor=\"let client of items\">\n            <ng-template [ngTemplateOutlet]=\"loadTemplate(client)\" \n                        [ngOutletContext]=\"{ $implicit: client}\">\n            </ng-template>\n        </tr>\n    </tbody>\n</table>\n<div>{{statusMessage}}</div>\n  \n  \n<!--\u0448\u0430\u0431\u043B\u043E\u043D \u0434\u043B\u044F \u0447\u0442\u0435\u043D\u0438\u044F-->\n<ng-template #readOnlyTemplate let-client>\n   \n    <td>{{client.FirstName}}</td>\n    <td>{{client.LastName}}</td>\n    <td>\n        <input type=\"button\" value=\"\u0418\u0437\u043C\u0435\u043D\u0438\u0442\u044C\" class=\"btn btn-default\" (click)=\"editClient(client)\" />\n    </td>\n    <td>\n        <input type=\"button\" value=\"\u0423\u0434\u0430\u043B\u0438\u0442\u044C\" (click)=\"deleteClient(client)\" class=\"btn btn-danger\" />\n    </td>\n</ng-template>\n \n<!--\u0448\u0430\u0431\u043B\u043E\u043D \u0434\u043B\u044F \u0440\u0435\u0434\u0430\u043A\u0442\u0438\u0440\u043E\u0432\u0430\u043D\u0438\u044F-->\n<ng-template #editTemplate>\n    \n    <td>\n        <input type=\"text\" [(ngModel)]=\"editedClient.FirstName\" class=\"form-control\" />\n    </td>\n    <td>\n        <input type=\"text\" [(ngModel)]=\"editedClient.LastName\" class=\"form-control\" />\n    </td>\n    <td>\n        <input type=\"button\" value=\"\u0421\u043E\u0445\u0440\u0430\u043D\u0438\u0442\u044C\" (click)=\"saveClient()\" class=\"btn btn-success\" />\n    </td>\n    <td>\n        <input type=\"button\" value=\"\u041E\u0442\u043C\u0435\u043D\u0430\" (click)=\"cancel()\" class=\"btn btn-warning\" />\n    </td>\n</ng-template>",
        providers: [http_service_1.HttpService]
    }),
    __metadata("design:paramtypes", [http_service_1.HttpService])
], AppComponent);
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map