"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var http_2 = require("@angular/http");
require("rxjs/add/operator/map");
var HttpService = (function () {
    function HttpService(http) {
        this.http = http;
    }
    HttpService.prototype.getClients = function () {
        return this.http.get('api/Client')
            .map(function (resp) {
            var clientList = resp.json();
            var clients = [];
            for (var index in clientList) {
                var cli = clientList[index];
                clients.push({ id: cli.id, FirstName: cli.firstName, LastName: cli.lastName });
            }
            return clients;
        });
    };
    HttpService.prototype.createClient = function (obj) {
        var body = JSON.stringify(obj);
        var headers = new http_2.Headers({ 'Content-Type': 'application/json;charset=utf-8' });
        return this.http.post('api/Client', body, { headers: headers });
    };
    HttpService.prototype.updateClient = function (id, obj) {
        var headers = new http_2.Headers({ 'Content-Type': 'application/json;charset=utf-8' });
        var body = JSON.stringify(obj);
        return this.http.put('api/Client/' + id, body, { headers: headers });
    };
    HttpService.prototype.deleteClient = function (id) {
        return this.http.delete('api/Client/' + id);
    };
    return HttpService;
}());
HttpService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], HttpService);
exports.HttpService = HttpService;
//# sourceMappingURL=http.service.js.map