﻿using Autofac;
using TestProjectWebAPI.Repository;

namespace TestProjectUI
{
    public class DefaultModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterGeneric(typeof(GenericRepository<>)).As(typeof(IGenericRepository<>)).InstancePerDependency();
        }
    }
}