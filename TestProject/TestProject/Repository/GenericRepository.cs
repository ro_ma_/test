﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using TestProjectWebAPI.Model;

namespace TestProjectWebAPI.Repository
{
    public class GenericRepository<Entity> : IGenericRepository<Entity> where Entity : class, new()
    {
        private TestContext dataContext;
        private DbSet<Entity> _entitySet;

        public TestContext Context
        {
            get { return dataContext ?? (dataContext = new TestContext()); }
        }

        public GenericRepository()
        {
            _entitySet = Context.Set<Entity>();
        }

        public void Add(Entity entity)
        {
            _entitySet.Add(entity);
            Context.SaveChanges();
        }

        public void Delete(int id)
        {
            Entity entity = _entitySet.Find(id);
            _entitySet.Attach(entity);
            Context.Entry(entity).State = EntityState.Deleted;
            Context.SaveChanges();
        }

        public virtual IEnumerable<Entity> Get(Expression<Func<Entity, bool>> filter = null)
        {
            if (filter != null)
            {
                return _entitySet.Where(filter);
            }
            return _entitySet.ToList();
        }

        public virtual Entity GetById(int id)
        {
            return _entitySet.Find(id);
        }

        public virtual void Update(Entity entity)
        {
            _entitySet.Attach(entity);
            Context.Entry(entity).State = EntityState.Modified;
            Context.SaveChanges();
        }
    }
}
