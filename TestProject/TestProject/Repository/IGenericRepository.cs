﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace TestProjectWebAPI.Repository
{
    public interface IGenericRepository<Entity> where Entity : class, new()
    {
        IEnumerable<Entity> Get(Expression<Func<Entity, bool>> filter = null);
        Entity GetById(int id);
        void Add(Entity entity);
        void Delete(int id);
        void Update(Entity entity);
    }
}
