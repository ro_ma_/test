﻿using System;
using System.Collections.Generic;

namespace TestProjectWebAPI.Model
{
    public partial class Products
    {
        public Products()
        {
            ClientProducts = new HashSet<ClientProducts>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }

        public virtual ICollection<ClientProducts> ClientProducts { get; set; }
    }
}
