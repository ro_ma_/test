﻿using System;
using System.Collections.Generic;

namespace TestProjectWebAPI.Model
{
    public partial class CurrencyRates
    {
        public int FkCurrencyIdFrom { get; set; }
        public int FkCurrencyIdTo { get; set; }
        public double Ratio { get; set; }
        public int Id { get; set; }

        public virtual Currencies FkCurrencyIdFromNavigation { get; set; }
        public virtual Currencies FkCurrencyIdToNavigation { get; set; }
    }
}
