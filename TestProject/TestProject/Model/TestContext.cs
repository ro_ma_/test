﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace TestProjectWebAPI.Model
{
    public partial class TestContext : DbContext
    {
        public virtual DbSet<ClientBalances> ClientBalances { get; set; }
        public virtual DbSet<ClientProducts> ClientProducts { get; set; }
        public virtual DbSet<Clients> Clients { get; set; }
        public virtual DbSet<Currencies> Currencies { get; set; }
        public virtual DbSet<CurrencyRates> CurrencyRates { get; set; }
        public virtual DbSet<Products> Products { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
           optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=Test;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ClientBalances>(entity =>
            {
                entity.HasKey(e => e.FkClientId)
                    .HasName("PK__client_b__223BC7CF8F761F11");

                entity.ToTable("client_balances");

                entity.Property(e => e.FkClientId)
                    .HasColumnName("fk_client_id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Amount)
                    .HasColumnName("amount")
                    .HasColumnType("money");

                entity.Property(e => e.FkCurrencyId).HasColumnName("fk_currency_id");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.HasOne(d => d.FkClient)
                    .WithOne(p => p.ClientBalances)
                    .HasForeignKey<ClientBalances>(d => d.FkClientId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_clientbalances_clients");

                entity.HasOne(d => d.FkCurrency)
                    .WithMany(p => p.ClientBalances)
                    .HasForeignKey(d => d.FkCurrencyId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_clientbalances_currencies");
            });

            modelBuilder.Entity<ClientProducts>(entity =>
            {
                entity.HasKey(e => new { e.FkClientId, e.FkProductsId })
                    .HasName("PK__client_p__5D8A59ADD6F85EAC");

                entity.ToTable("client_products");

                entity.Property(e => e.FkClientId).HasColumnName("fk_client_id");

                entity.Property(e => e.FkProductsId).HasColumnName("fk_products_id");

                entity.Property(e => e.Amount)
                    .HasColumnName("amount")
                    .HasDefaultValueSql("1");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.HasOne(d => d.FkClient)
                    .WithMany(p => p.ClientProducts)
                    .HasForeignKey(d => d.FkClientId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_clientproducts_clients");

                entity.HasOne(d => d.FkProducts)
                    .WithMany(p => p.ClientProducts)
                    .HasForeignKey(d => d.FkProductsId)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_clientproducts_products");
            });

            modelBuilder.Entity<Clients>(entity =>
            {
                entity.ToTable("clients");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasColumnName("first_name")
                    .HasMaxLength(255);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasColumnName("last_name")
                    .HasMaxLength(255);
            });

            modelBuilder.Entity<Currencies>(entity =>
            {
                entity.ToTable("currencies");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<CurrencyRates>(entity =>
            {
                entity.HasKey(e => new { e.FkCurrencyIdFrom, e.FkCurrencyIdTo })
                    .HasName("PK__currency__2DF2155F1372FBAB");

                entity.ToTable("currency_rates");

                entity.Property(e => e.FkCurrencyIdFrom).HasColumnName("fk_currency_id_from");

                entity.Property(e => e.FkCurrencyIdTo).HasColumnName("fk_currency_id_to");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Ratio).HasColumnName("ratio");

                entity.HasOne(d => d.FkCurrencyIdFromNavigation)
                    .WithMany(p => p.CurrencyRatesFkCurrencyIdFromNavigation)
                    .HasForeignKey(d => d.FkCurrencyIdFrom)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_currencyratesfrom_currencies");

                entity.HasOne(d => d.FkCurrencyIdToNavigation)
                    .WithMany(p => p.CurrencyRatesFkCurrencyIdToNavigation)
                    .HasForeignKey(d => d.FkCurrencyIdTo)
                    .OnDelete(DeleteBehavior.Restrict)
                    .HasConstraintName("FK_currencyratesto_currencies");
            });

            modelBuilder.Entity<Products>(entity =>
            {
                entity.ToTable("products");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Description).HasColumnName("description");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50);

                entity.Property(e => e.Price)
                    .HasColumnName("price")
                    .HasColumnType("money");
            });
        }
    }
}