﻿using System;
using System.Collections.Generic;

namespace TestProjectWebAPI.Model
{
    public partial class Currencies
    {
        public Currencies()
        {
            ClientBalances = new HashSet<ClientBalances>();
            CurrencyRatesFkCurrencyIdFromNavigation = new HashSet<CurrencyRates>();
            CurrencyRatesFkCurrencyIdToNavigation = new HashSet<CurrencyRates>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<ClientBalances> ClientBalances { get; set; }
        public virtual ICollection<CurrencyRates> CurrencyRatesFkCurrencyIdFromNavigation { get; set; }
        public virtual ICollection<CurrencyRates> CurrencyRatesFkCurrencyIdToNavigation { get; set; }
    }
}
