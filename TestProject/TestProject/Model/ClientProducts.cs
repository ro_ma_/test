﻿using System;
using System.Collections.Generic;

namespace TestProjectWebAPI.Model
{
    public partial class ClientProducts
    {
        public int FkClientId { get; set; }
        public int FkProductsId { get; set; }
        public int Amount { get; set; }
        public int Id { get; set; }

        public virtual Clients FkClient { get; set; }
        public virtual Products FkProducts { get; set; }
    }
}
