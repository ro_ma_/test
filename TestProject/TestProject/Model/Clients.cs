﻿using System;
using System.Collections.Generic;

namespace TestProjectWebAPI.Model
{
    public partial class Clients
    {
        public Clients()
        {
            ClientProducts = new HashSet<ClientProducts>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public virtual ClientBalances ClientBalances { get; set; }
        public virtual ICollection<ClientProducts> ClientProducts { get; set; }
    }
}
