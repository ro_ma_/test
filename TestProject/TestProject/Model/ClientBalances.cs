﻿using System;
using System.Collections.Generic;

namespace TestProjectWebAPI.Model
{
    public partial class ClientBalances
    {
        public int Id { get; set; }
        public int FkClientId { get; set; }
        public int FkCurrencyId { get; set; }
        public decimal Amount { get; set; }

        public virtual Clients FkClient { get; set; }
        public virtual Currencies FkCurrency { get; set; }
    }
}
