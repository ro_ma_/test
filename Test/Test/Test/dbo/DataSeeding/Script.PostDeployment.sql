﻿/*
Шаблон скрипта после развертывания							
--------------------------------------------------------------------------------------
 В данном файле содержатся инструкции SQL, которые будут добавлены в скрипт построения.		
 Используйте синтаксис SQLCMD для включения файла в скрипт после развертывания.			
 Пример:      :r .\myfile.sql								
 Используйте синтаксис SQLCMD для создания ссылки на переменную в скрипте после развертывания.		
 Пример:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

SET IDENTITY_INSERT dbo.products ON
INSERT INTO dbo.products(id, name, description, price) VALUES (1, 'Product 1', 'Description of the product 1', 1)
INSERT INTO dbo.products(id, name, description, price) VALUES (2, 'Product 2', 'Description of the product 2', 2)
INSERT INTO dbo.products(id, name, description, price) VALUES (3, 'Product 3', 'Description of the product 3', 3)
SET IDENTITY_INSERT dbo.products OFF
GO

SET IDENTITY_INSERT dbo.clients ON
INSERT INTO dbo.clients(id, first_name, last_name) VALUES (1, 'Ivan', 'Ivanov')
INSERT INTO dbo.clients(id, first_name, last_name) VALUES (2, 'Petr', 'Sidorov')
INSERT INTO dbo.clients(id, first_name, last_name) VALUES (3, 'Sidor', 'Petrov')
SET IDENTITY_INSERT dbo.clients OFF
GO

SET IDENTITY_INSERT dbo.currencies ON
INSERT INTO dbo.currencies(id, name) VALUES (1, 'Pound')
INSERT INTO dbo.currencies(id, name) VALUES (2, 'Dollar')
INSERT INTO dbo.currencies(id, name) VALUES (3, 'Rubl')
SET IDENTITY_INSERT dbo.currencies OFF
GO

SET IDENTITY_INSERT dbo.currency_rates ON
INSERT INTO dbo.currency_rates(id, fk_currency_id_from, fk_currency_id_to, ratio) VALUES (1, 1, 2, 1.2)
INSERT INTO dbo.currency_rates(id, fk_currency_id_from, fk_currency_id_to, ratio) VALUES (2, 1, 3, 70)
INSERT INTO dbo.currency_rates(id, fk_currency_id_from, fk_currency_id_to, ratio) VALUES (3, 2, 3, 60)
SET IDENTITY_INSERT dbo.currency_rates OFF
GO

SET IDENTITY_INSERT dbo.client_balances ON
INSERT INTO dbo.client_balances(id, fk_client_id, fk_currency_id, amount) VALUES (1, 1, 1, 10)
INSERT INTO dbo.client_balances(id, fk_client_id, fk_currency_id, amount) VALUES (1, 2, 2, 20)
INSERT INTO dbo.client_balances(id, fk_client_id, fk_currency_id, amount) VALUES (2, 3, 3, 30)
SET IDENTITY_INSERT dbo.client_balances OFF
GO

SET IDENTITY_INSERT dbo.client_products ON
INSERT INTO dbo.client_products (id, fk_client_id, fk_products_id, amount) VALUES (1, 1, 1, 1)
INSERT INTO dbo.client_products (id, fk_client_id, fk_products_id, amount) VALUES (2, 1, 2, 2)
INSERT INTO dbo.client_products (id, fk_client_id, fk_products_id, amount) VALUES (3, 1, 3, 4)
INSERT INTO dbo.client_products (id, fk_client_id, fk_products_id, amount) VALUES (4, 2, 1, 2)
INSERT INTO dbo.client_products (id, fk_client_id, fk_products_id, amount) VALUES (5, 2, 3, 3)
INSERT INTO dbo.client_products (id, fk_client_id, fk_products_id, amount) VALUES (6, 3, 1, 10)
INSERT INTO dbo.client_products (id, fk_client_id, fk_products_id, amount) VALUES (7, 3, 2, 100)
SET IDENTITY_INSERT dbo.client_products OFF
GO
