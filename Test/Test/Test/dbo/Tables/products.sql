﻿CREATE TABLE [dbo].[products]
(
	[id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [name] NVARCHAR(50) NOT NULL, 
    [description] NVARCHAR(MAX) NULL, 
    [price] MONEY NOT NULL
)
