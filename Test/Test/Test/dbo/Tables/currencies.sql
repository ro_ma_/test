﻿CREATE TABLE [dbo].[currencies]
(
	[id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [name] NVARCHAR(50) NOT NULL
)
