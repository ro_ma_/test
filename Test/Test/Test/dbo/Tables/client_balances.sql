﻿CREATE TABLE [dbo].[client_balances]
(
	[id] INT NOT NULL IDENTITY,
	[fk_client_id] INT NOT NULL PRIMARY KEY, 
    [fk_currency_id] INT NOT NULL, 
    [amount] MONEY NOT NULL,
	
	CONSTRAINT FK_clientbalances_clients FOREIGN KEY (fk_client_id) REFERENCES clients (id),
	CONSTRAINT FK_clientbalances_currencies FOREIGN KEY (fk_currency_id) REFERENCES currencies (id) 
)
