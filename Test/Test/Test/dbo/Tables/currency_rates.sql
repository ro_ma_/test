﻿CREATE TABLE [dbo].[currency_rates]
(
	[fk_currency_id_from] INT NOT NULL , 
    [fk_currency_id_to] INT NOT NULL, 
    [ratio] FLOAT NOT NULL, 
    [id] INT NOT NULL IDENTITY, 
    PRIMARY KEY ([fk_currency_id_from], [fk_currency_id_to]),

	CONSTRAINT FK_currencyratesfrom_currencies FOREIGN KEY (fk_currency_id_from) REFERENCES currencies (id), 
	CONSTRAINT FK_currencyratesto_currencies FOREIGN KEY (fk_currency_id_to) REFERENCES currencies (id) 
)
