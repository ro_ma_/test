﻿CREATE TABLE [dbo].[client_products]
(
	[fk_client_id] INT NOT NULL , 
    [fk_products_id] INT NOT NULL, 
    [amount] INT NOT NULL DEFAULT 1, 
    [id] INT NOT NULL IDENTITY, 
    PRIMARY KEY ([fk_client_id], [fk_products_id]),

	CONSTRAINT FK_clientproducts_clients FOREIGN KEY (fk_client_id) REFERENCES clients (id), 
	CONSTRAINT FK_clientproducts_products FOREIGN KEY (fk_products_id) REFERENCES products (id) 

)
